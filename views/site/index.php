<?php

/**
 * @var $this yii\web\View
 * @var $model \app\models\User
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\DetailView;

$this->title = 'Профиль пользователя';

?>
<div class="site-index">

    <div class="jumbotron">

        <h1>Профиль</h1>

        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
            'layout' => 'horizontal',
        ]); ?>

        <?= $form->field($model, 'first_name')->textInput(); ?>

        <?= $form->field($model, 'middle_name')->textInput(); ?>

        <?= $form->field($model, 'last_name')->textInput(); ?>

        <?= Html::submitButton('Сохранить'); ?>

        <?php ActiveForm::end(); ?>

        <br>

        <?php
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'first_name',
                'middle_name',
                'last_name',
                'email',
                'bonus',
                'api_key',
            ],
        ]);
        ?>

    </div>

</div>
