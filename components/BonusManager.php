<?php

namespace app\components;

use app\models\User;
use yii\base\Component;

/**
 * Class BonusManager
 * @package app\components
 */
class BonusManager extends Component
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * UserBonusManager constructor.
     *
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * @param int $bonus
     *
     * @return bool
     */
    public function increase(int $bonus): bool
    {
        $this->user->bonus += $bonus;

        return $this->save();
    }

    /**
     * @return bool
     */
    protected function save(): bool
    {
        if (!$this->user->save()) {
            $this->addError($this->user->getErrorSummary(false));
        }

        return true;
    }

    /**
     * @param int $bonus
     *
     * @return bool
     */
    public function decrease(int $bonus): bool
    {
        if ($this->user->bonus < $bonus) {
            $this->addError('Не удалось списать бонусы: кризис!');

            return false;
        }

        $this->user->bonus -= $bonus;

        return $this->save();
    }

    /**
     * @param string $error
     *
     * @return $this
     */
    public function addError(string $error)
    {
        $this->errors[] = $error;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastError(): string
    {
        if (!empty($this->errors)) {
            return array_pop($this->errors);
        }

        return '';
    }
}
