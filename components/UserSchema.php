<?php

namespace app\components;

use app\models\User;
use Neomerx\JsonApi\Schema\BaseSchema;

/**
 * Class UserSchema
 * @package app\models
 */
class UserSchema extends BaseSchema
{
    /**
     * @inheritdoc
     */
    protected $resourceType = 'user';

    /**
     * @param User $user
     *
     * @return null|string
     */
    public function getId($user): ?string
    {
        return $user->getPrimaryKey();
    }

    /**
     * @param User $user
     * @param array|null $fieldKeysFilter
     *
     * @return array|null
     */
    public function getAttributes($user, array $fieldKeysFilter = null): ?array
    {
        return [
            'first_name' => $user->first_name,
            'middle_name' => $user->middle_name,
            'last_name' => $user->last_name,
            'bonus' => $user->bonus,
            'email' => $user->email,
        ];
    }
}
