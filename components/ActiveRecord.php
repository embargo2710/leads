<?php

namespace app\components;

/**
 * Class ActiveRecord
 * @package app\components
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 0;

    const STATUS_DELETED = -1;
}
