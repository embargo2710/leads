<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class LoginForm
 * @package app\models
 */
class LoginForm extends Model
{
    const SCENARIO_AUTH = 'auth';

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $auth_key;

    /**
     * @var User
     */
    protected $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['email'], 'email'],
            [['auth_key'], 'required', 'on' => [self::SCENARIO_AUTH]],
            [['auth_key'], 'string', 'length' => 32, 'on' => [self::SCENARIO_AUTH]],
        ];
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function sendAuthEmail(): bool
    {
        return \Yii::$app->mailer->compose()
            ->setFrom('admin@artika.studio')
            ->setTo($this->user->email)
            ->setSubject('Ссылка для входа')
            ->setHtmlBody(Html::a(
                'Вход',
                Url::to(['/site/auth', 'auth_key' => $this->user->getAuthKey()], true)
                )
            )
            ->send();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function createOrUpdateUser()
    {
        $this->user = $this->getUser();

        if (!($this->user instanceof User)) {
            $this->user = $this->createUser();
        }

        if (!$this->user->generateAuthKey()->save()) {
            throw new \Exception('Не удалось сохранить ключ авторизации для пользователя');
        }

        return $this;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    protected function getUser()
    {
        if (!$this->user) {
            $this->user = User::findByEmail($this->email)->one();
        }

        return $this->user;
    }

    /**
     * @return User
     * @throws \Exception
     */
    protected function createUser()
    {
        $user = new User(['email' => $this->email]);

        if (!$user->save()) {
            throw new \Exception('Не удалось сохранить модель User');
        }

        return $user;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function login()
    {
        if ($this->validate()) {
            $this->user = \Yii::$app->user->loginByAccessToken($this->auth_key);

            if (!$this->user instanceof User) {
                return false;
            }

            $this->user->generateAuthKey();

            if (!$this->user->save()) {
                throw new \Exception('Не удалось сохранить пользователя');
            }

            return true;
        }

        return false;
    }
}
