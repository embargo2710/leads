<?php

namespace app\models;

use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * Class User
 * @package app\models
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    const BONUS_DEFAULT = 1000;

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->api_key = \Yii::$app->security->generateRandomString();
            $this->bonus = self::BONUS_DEFAULT;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['email', 'api_key'],
                'required',
                'on' => ['insert'],
            ],
            [
                ['first_name', 'last_name', 'middle_name'],
                'string'
            ],
            [
                ['first_name', 'last_name', 'middle_name'],
                'trim'
            ],
            [
                ['status'],
                'default',
                'value' => self::STATUS_ACTIVE,
            ],
            [
                ['auth_key'],
                'string',
                'length' => 32,
            ],
            [
                ['bonus'],
                'default',
                'value' => self::BONUS_DEFAULT,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'last_name' => 'Фамилия',
            'email' => 'E-mail',
            'status' => 'Статус',
            'auth_key' => 'Ключ авторизации',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'bonus' => 'Бонус',
            'api_key' => 'Ваш API-ключ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::find()->where(['id' => $id, 'status' => self::STATUS_ACTIVE])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->andWhere(['auth_key' => $token])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->primaryKey;
    }

    /**
     * @param string $email
     *
     * @return ActiveQuery
     */
    public static function findByEmail(string $email)
    {
        return self::find()->where(['email' => $email]);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();

        return $this;
    }

    /**
     * @param string $apiKey
     *
     * @return bool
     */
    public function validateApiKey(string $apiKey): bool
    {
        return $this->api_key === $apiKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
