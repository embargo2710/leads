<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180130_114553_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(100)->notNull(),
            'first_name' => $this->string(20)->null(),
            'middle_name' => $this->string(20)->null(),
            'last_name' => $this->string(20)->null(),
            'auth_key' => $this->string(32)->null(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'status' => $this->smallInteger(2)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
