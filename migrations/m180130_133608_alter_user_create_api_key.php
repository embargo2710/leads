<?php

use yii\db\Migration;

/**
 * Class m180130_133608_alter_user_create_api_key
 */
class m180130_133608_alter_user_create_api_key extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'api_key', $this->string(32)->null());

        $this->addColumn('user', 'bonus', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'api_key');

        $this->dropColumn('user', 'bonus');
    }
}
