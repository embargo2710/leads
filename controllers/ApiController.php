<?php

namespace app\controllers;

use app\components\BonusManager;
use app\components\UserSchema;
use app\models\User;
use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-info', 'decrease-bonus', 'increase-bonus'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-info' => ['get'],
                    'decrease-bonus' => ['get'],
                    'increase-bonus' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @param int $id
     * @param string $api_key
     *
     * @return string
     */
    public function actionGetInfo(string $api_key)
    {
        $model = $this->getUser($api_key);

        $encoder = Encoder::instance([
            User::class => UserSchema::class,
        ], new EncoderOptions(JSON_PRETTY_PRINT));

        if (!($model instanceof User)) {
            return $encoder->encodeError($this->getUserError());
        }

        return $encoder->encodeData([$model]);
    }

    /**
     * @param string $api_key
     *
     * @return null|static
     */
    protected function getUser(string $api_key)
    {
        return User::findOne(['api_key' => $api_key, 'status' => User::STATUS_ACTIVE]);
    }

    /**
     * @return Error
     */
    protected function getUserError()
    {
        return new Error(
            null,
            null,
            'Not found',
            '404',
            'User not found'
        );
    }

    /**
     * @param string $api_key
     * @param int $bonus
     *
     * @return string
     */
    public function actionDecreaseBonus(string $api_key, int $bonus)
    {
        $model = $this->getUser($api_key);

        $encoder = Encoder::instance([], new EncoderOptions());

        if (!($model instanceof User)) {
            return $encoder->encodeError($this->getUserError());
        }

        $bonusManager = new BonusManager($model);

        if (!$bonusManager->decrease($bonus)) {
            return $encoder->encodeError(new Error(
                null,
                null,
                'Not Acceptable',
                '406',
                $bonusManager->getLastError()
            ));
        }

        return $encoder->encodeMeta([
            'success' => [
                'title' => 'Успех',
                'status' => 200,
                'message' => 'Бонус успешно списан'
            ]
        ]);
    }

    /**
     * @param string $api_key
     * @param int $bonus
     *
     * @return string
     */
    public function actionIncreaseBonus(string $api_key, int $bonus)
    {
        $model = $this->getUser($api_key);

        $encoder = Encoder::instance([], new EncoderOptions());

        if (!($model instanceof User)) {
            return $encoder->encodeError($this->getUserError());
        }

        $bonusManager = new BonusManager($model);

        if (!$bonusManager->increase($bonus)) {
            return $encoder->encodeError(new Error(
                null,
                null,
                'Not Acceptable',
                '406',
                $bonusManager->getLastError()
            ));
        }

        return $encoder->encodeMeta([
            'success' => [
                'title' => 'Успех',
                'status' => 200,
                'message' => 'Бонус успешно начислен'
            ]
        ]);
    }
}
